

var gouraudShader, textureShader;

function initShaders() {
	var vertexShader = getShader(gl, "gouraud-vs");
	var fragmentShader = getShader(gl, "gouraud-fs");
	
	gouraudShader = gl.createProgram();
	gl.attachShader(gouraudShader, vertexShader);
	gl.attachShader(gouraudShader, fragmentShader);
	gl.linkProgram(gouraudShader);
	
	if (!gl.getProgramParameter(gouraudShader, gl.LINK_STATUS)) {
		alert("Could not initialise shaders");
	}
	
	gouraudShader.vertexPositionAttribute = gl.getAttribLocation(gouraudShader, "aVertexPosition");
	gl.enableVertexAttribArray(gouraudShader.vertexPositionAttribute);
	
	gouraudShader.vertexColorAttribute = gl.getAttribLocation(gouraudShader, "aVertexColor");
	gl.enableVertexAttribArray(gouraudShader.vertexColorAttribute);
	
	gouraudShader.pMatrixUniform = gl.getUniformLocation(gouraudShader, "uPMatrix");
	gouraudShader.mvMatrixUniform = gl.getUniformLocation(gouraudShader, "uMVMatrix");
	
	//texture shader attribs
	vertexShader = getShader(gl, "texture-vs");
	fragmentShader = getShader(gl, "texture-fs");
	textureShader = gl.createProgram();
	gl.attachShader(textureShader, vertexShader);
	gl.attachShader(textureShader, fragmentShader);
	gl.linkProgram(textureShader);
	
	if (!gl.getProgramParameter(textureShader, gl.LINK_STATUS)) {
		alert("Could not initialise shaders");
	}
	
	textureShader.vertexPositionAttribute = gl.getAttribLocation(textureShader, "aVertexPosition");
	gl.enableVertexAttribArray(textureShader.vertexPositionAttribute);
	
	textureShader.textureCoordAttribute = gl.getAttribLocation(textureShader, "aTextureCoord");
	gl.enableVertexAttribArray(textureShader.textureCoordAttribute);
	
	textureShader.vertexColorAttribute = gl.getAttribLocation(textureShader, "aVertexColor");
	gl.enableVertexAttribArray(textureShader.vertexColorAttribute);
	
	textureShader.pMatrixUniform = gl.getUniformLocation(textureShader, "uPMatrix");
	textureShader.mvMatrixUniform = gl.getUniformLocation(textureShader, "uMVMatrix");
	textureShader.samplerUniform = gl.getUniformLocation(textureShader, "uSampler");
}


var textures;// = [];
function initTextures(){
	textures = gl.createTexture();
	textures.image = new Image();
	textures.image.onload = function(){
		handleLoadedTexture(textures)
	}
	textures.image.src = "assets/donut1.png";
}

function handleLoadedTexture(texture){
	gl.bindTexture(gl.TEXTURE_2D, texture);
	gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
	gl.bindTexture(gl.TEXTURE_2D, null);
}


var mvMatrix = mat4.create();
var pMatrix = mat4.create();

function setMatrixUniforms(shader) {
	gl.uniformMatrix4fv(shader.pMatrixUniform, false, pMatrix);
	gl.uniformMatrix4fv(shader.mvMatrixUniform, false, mvMatrix);
}

var triangleVertexPositionBuffer;
var triangleVertexColorBuffer;
var squareVertexPositionBuffer;
var squareVertexColorBuffer;
var squareVertexTextureCoordBuffer;
var squareVertexIndexBuffer;

function initBuffers() {
	triangleVertexPositionBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, triangleVertexPositionBuffer);
	var vertices = [
		0.0,  1.0,  0.0,
		-1.0, -1.0,  1.0,
		1.0, -1.0,  1.0,
		
		1.0, -1.0,  1.0,
		1.0, -1.0,  -1.0,
		0.0, 1.0,  0.0,
	];

	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
	triangleVertexPositionBuffer.itemSize = 3;
	triangleVertexPositionBuffer.numItems = 6;
	
	triangleVertexColorBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, triangleVertexColorBuffer);
	var colors = [
		1.0, 0.0, 0.0, 1.0,
		0.0, 1.0, 0.0, 1.0,
		0.0, 0.0, 1.0, 1.0,
		
		1.0, 0.0, 0.0, 1.0,
		1.0, 1.0, 0.0, 1.0,
		1.0, 0.0, 1.0, 1.0
	];
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);
	triangleVertexColorBuffer.itemSize = 4;
	triangleVertexColorBuffer.numItems = 3;
	
	squareVertexPositionBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, squareVertexPositionBuffer);
	vertices = [
		-1.0, -1.0,  1.0,
		1.0, -1.0,  1.0,
		1.0,  1.0,  1.0,
		-1.0,  1.0,  1.0
	];
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
	squareVertexPositionBuffer.itemSize = 3;
	squareVertexPositionBuffer.numItems = 4;
	
	squareVertexTextureCoordBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, squareVertexTextureCoordBuffer);
	var textureCoords = [
		// Front face
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
	];
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCoords), gl.STATIC_DRAW);
	squareVertexTextureCoordBuffer.itemSize = 2;
	squareVertexTextureCoordBuffer.numItems = textureCoords.length/2;//24;
	
	squareVertexIndexBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, squareVertexIndexBuffer);
	var squareVertexIndices = [
		0, 1, 2,	0, 2, 3
	];
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(squareVertexIndices), gl.STATIC_DRAW);
	squareVertexIndexBuffer.itemSize = 1;
	squareVertexIndexBuffer.numItems = squareVertexIndices.length;
	
	squareVertexColorBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, squareVertexColorBuffer);
	colors = []
	for (var i=0; i < 4; i++) {
		colors = colors.concat([i/4.0, i/4.0, i/4.0, 1.0]);
	}
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);
	squareVertexColorBuffer.itemSize = 4;
	squareVertexColorBuffer.numItems = colors.length;
}

var dz = -3.0;
function drawScene() {
	gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	mat4.perspective(45, gl.viewportWidth / gl.viewportHeight, 0.1, 100.0, pMatrix);
	
	mat4.identity(mvMatrix);
	
	dz -= 0.1;
	mat4.translate(mvMatrix, [-1.5, 0.0, -17.0]);
	mat4.rotate(mvMatrix, dz/10, [0, 1, 0], mvMatrix);
	
	gl.useProgram(gouraudShader);
	gl.bindBuffer(gl.ARRAY_BUFFER, triangleVertexPositionBuffer);
	gl.vertexAttribPointer(gouraudShader.vertexPositionAttribute, triangleVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, triangleVertexColorBuffer);
	gl.vertexAttribPointer(gouraudShader.vertexColorAttribute, triangleVertexColorBuffer.itemSize, gl.FLOAT, false, 0, 0);
	
	setMatrixUniforms(gouraudShader);
	gl.drawArrays(gl.TRIANGLES, 0, triangleVertexPositionBuffer.numItems);
	
	mat4.translate(mvMatrix, [0.0, -2.0, 0.0]);
	mat4.scale(mvMatrix, [10, 0, 10], mvMatrix);
	mat4.rotate(mvMatrix, Math.PI/2, [-1, 0, 0]);
	
	gl.useProgram(textureShader);
	gl.bindBuffer(gl.ARRAY_BUFFER, squareVertexPositionBuffer);
	gl.vertexAttribPointer(textureShader.vertexPositionAttribute, squareVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, squareVertexTextureCoordBuffer);
	gl.vertexAttribPointer(textureShader.textureCoordAttribute, squareVertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);
	
	gl.bindBuffer(gl.ARRAY_BUFFER, squareVertexColorBuffer);
	gl.vertexAttribPointer(textureShader.vertexColorAttribute, squareVertexColorBuffer.itemSize, gl.FLOAT, false, 0, 0);
	
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, textures);
	gl.uniform1i(textureShader.samplerUniform, 0);
	
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, squareVertexIndexBuffer);
	setMatrixUniforms(textureShader);
	gl.drawElements(gl.TRIANGLES, squareVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
}

function enterFrame(){
	requestAnimationFrame(enterFrame);
	drawScene();
}

function webGLStart() {
	var canvas = document.getElementById("mycanvas");
	initGL(canvas);
	initShaders();
	initBuffers();
	initTextures();
	
	gl.clearColor(0.68, 0.82, 0.96, 1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.enable(gl.CULL_FACE);
	
	enterFrame();
}

